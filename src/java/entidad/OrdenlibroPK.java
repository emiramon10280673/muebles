/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Daftzero
 */
@Embeddable
public class OrdenlibroPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "orden_id")
    private int ordenId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "libro_id")
    private int libroId;

    public OrdenlibroPK() {
    }

    public OrdenlibroPK(int ordenId, int libroId) {
        this.ordenId = ordenId;
        this.libroId = libroId;
    }

    public int getOrdenId() {
        return ordenId;
    }

    public void setOrdenId(int ordenId) {
        this.ordenId = ordenId;
    }

    public int getLibroId() {
        return libroId;
    }

    public void setLibroId(int libroId) {
        this.libroId = libroId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ordenId;
        hash += (int) libroId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenlibroPK)) {
            return false;
        }
        OrdenlibroPK other = (OrdenlibroPK) object;
        if (this.ordenId != other.ordenId) {
            return false;
        }
        if (this.libroId != other.libroId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.OrdenlibroPK[ ordenId=" + ordenId + ", libroId=" + libroId + " ]";
    }
    
}
